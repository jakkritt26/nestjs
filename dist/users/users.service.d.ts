import { Repository } from 'typeorm';
import { Users } from "./../users/users.entity";
export declare class UsersService {
    private userRepo;
    private returnData;
    constructor(userRepo: Repository<Users>);
    findAll(): Promise<Users[]>;
    findOne(id: number): Promise<Users>;
    create(body: any): Promise<Users[]>;
    update(id: number, body: any): Promise<Users>;
    delete(id: number): Promise<import("typeorm").DeleteResult>;
}
