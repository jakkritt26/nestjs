import { BaseEntity } from "../base-entity";
export declare class Users extends BaseEntity {
    username: string;
    password: string;
    name: string;
    email: string;
}
