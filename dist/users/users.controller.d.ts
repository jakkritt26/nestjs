import { UsersService } from './users.service';
export declare class UsersController {
    private readonly UsersService;
    constructor(UsersService: UsersService);
    findAll(): Promise<import("./users.entity").Users[]>;
    findOne(id: number): Promise<import("./users.entity").Users>;
    create(body: any): Promise<import("./users.entity").Users[]>;
    update(id: number, body: any): Promise<import("./users.entity").Users>;
    remove(id: number): Promise<import("typeorm").DeleteResult>;
}
