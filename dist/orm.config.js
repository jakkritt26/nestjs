"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = void 0;
exports.config = {
    type: 'postgres',
    host: 'localhost',
    username: 'admin',
    password: 'secret',
    port: 5432,
    database: 'postgres',
    synchronize: true,
    entities: ['dist/**/*.entity{.ts,.js}']
};
//# sourceMappingURL=orm.config.js.map