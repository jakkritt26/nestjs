import { CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";


export class BaseEntity {
       // auto incressment
       @PrimaryGeneratedColumn()
       id?: string;

       @CreateDateColumn({nullable:true})
       createdAt?: Date;

       @UpdateDateColumn({nullable:true})
       updatedAt?: Date;
}