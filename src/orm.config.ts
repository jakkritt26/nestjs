import { TypeOrmModuleOptions } from "@nestjs/typeorm";

export const config: TypeOrmModuleOptions = {
    type: 'postgres',
    host: 'localhost',
    username: 'admin',
    password: 'secret',
    port: 5432,
    database: 'postgres',
    synchronize: true,
    entities: ['dist/**/*.entity{.ts,.js}']
};