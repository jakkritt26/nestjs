import {  Column, Entity } from "typeorm";
import { BaseEntity } from "../base-entity";

@Entity('users')

export class Users extends BaseEntity{
    // auto incressment
    @Column({length:255})
    username: string;

    @Column({length:255})
    password: string;

    @Column({length:255})
    name: string;

    @Column({length:255})
    email: string;

}