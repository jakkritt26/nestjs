import { Controller, Get, Param, Post, Delete, HttpCode, Put, Body, Res } from '@nestjs/common';
import { UsersService } from './users.service';


@Controller('users')
export class UsersController {

    constructor(private readonly UsersService: UsersService) {}

    @Get()
    @HttpCode(200)
    findAll() {
        return this.UsersService.findAll()
        //return res.json({ result: 200});
    }

    @Get(':id')
    @HttpCode(200)
    findOne(@Param('id') id: number) {
        return this.UsersService.findOne(id);
    }

    @Post()
    @HttpCode(204)
    create(@Body() body:any) {

        return this.UsersService.create(body);
    }

    @Put(":id")
    update(@Param('id') id: number, @Body() body:any) {
        return this.UsersService.update(id, body);
    }

    @Delete(":id")
    remove(@Param('id') id: number) {
        return this.UsersService.delete(id);
    }

}
