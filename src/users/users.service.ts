import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Users } from "./../users/users.entity";

@Injectable()
export class UsersService {

    private returnData : [] = [];
    constructor(
        @InjectRepository(Users) private userRepo: Repository<Users>
    ){ }

    findAll() {
        return this.userRepo.find();
      }

    findOne(id: number) {
        return this.userRepo.findOne(id);
    }

    async create(body: any) {
        const newUser = await this.userRepo.create(body);
        return this.userRepo.save(newUser);
    }

    async update(id: number, body: any){
        const user = await this.userRepo.findOne(id);
        this.userRepo.merge(user, body);
        return this.userRepo.save(user);
    }

    delete(id: number){
        return this.userRepo.delete(id)
    }
}
