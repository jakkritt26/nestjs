import {  Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatModule } from './cat/cat.module';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { config } from "./orm.config";
@Module({

  imports: [CatModule, UsersModule, TypeOrmModule.forRoot(config)],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
